## Mis APKBUILD

![Alpine linux logo](https://alpinelinux.org/alpinelinux-logo.svg)


Es un repositorio de `APKBUILD` (recetas) para hacer paquetes para [Alpine linux] (https://alpinelinux.org/)
tambien para configurar `abuild` para que puedas crear los paquetes a partir de los `APKBUILD`.

Si detecta algun error en los APKBUILD por favor avisar en los [Issues](https://gitea.com/Sodomon/Mis_APKBUILD/issues) 

### Como instalar abuild

Para hacer los paquetes necesitaremos abuild intalado
abriremos una terminal y ejecutaremos el comando ,`sudo apk add abuild`, debe tener configurado `sudo`

```bash
sudo apk add abuild
```

**para hacerle la vida más fácil a la hora de empaquetar, es recomendable crear un nuevo usuario**

`adduser <tuusuario>`

luego de haber creado dicho usuario, debe darle permiso en `/etc/sudoers`
añada la línea usando el comando `visudo`:

`<tuusuario> ALL=(ALL) ALL` una línea por debajo de

User privilege specification

``root ALL=(ALL) ALL``

Ahora cierre la sesión de la cuenta de root, e inicie sesión como `<tuusuario>`. A partir de aquí todo se puede hacer en una cuenta de usuario normal, y las operaciones que requieren privilegios de superusuario se pueden hacer con sudo.

### Configurando git

Debe configurar git en su nueva sesion de usuario

`git config --global user.name "tu nombre completo"`

`git config --global user.email "tuusario@tucorreoelectronico.com"``

Antes de empezar a crear o modificar archivos APKBUILD, necesitamos darle permisos de `abuild` al usuario creado.
Edite el archivo abuild.conf según sus necesidades, desde la terminal:

`sudo addgroup <tuusuario> abuild`

También necesitamos preparar la ubicación donde el proceso de compilación almacena
en caché los archivos que se descargan, por defecto es `/var/cache/distfiles/`, para crear este directorio y asegurarse de 
que tiene permisos de escritura, introduzca los siguientes comandos:

`sudo mkdir -p /var/cache/distfiles`

`sudo chmod a+w /var/cache/distfiles`

`sudo chgrp abuild /var/cache/distfiles`

`sudo chmod g+w /var/cache/distfiles`

El último paso es configurar las claves de seguridad con el script `abuild-keygen` para abuild con el comando: 

`abuild-keygen -a -i`

En versiones anteriores de Alpine, teníamos que crear manualmente claves para firmar paquetes e índices. Esto explica cómo, hoy en día se puede usar `abuild-keygen`.
Dado que la clave pública debe ser única para cada desarrollador, la dirección de correo electrónico debe utilizarse como nombre de la clave pública. 

#### Creando una llave privada

`openssl genrsa -out tucorreoelectronico.priv 2048`

#### Creando una llave publica

`openssl rsa -in tucorreoelectronico.priv -pubout -out /etc/apk/keys/tucorreoelectronico`

La llave pública debe ser distribuida e instalada en /etc/apk/keys la caja de alpine 
que instalará los paquetes, esto significa básicamente que las llaves públicas del desarrollador principal 
deberían estar en /etc/apk/keys en todas las cajas Alpine.

### Para crear los paquetes con abuild

Entraremos en la carpeta donde estan los paquetes con el comando `cd`,
dentro de la carpeta de los paquetes usaremos. 

`cd nombre del paquete`.

Ya adentro de la carpeta del nombre del paquete ejecutaremos el comando `abuild`.

### Contactos
- Email: sodomon2@gmail.com
- Facebook: https://www.facebook.com/diego.diazurbaneja.5
- Github: https://github.com/sodomon2

### NOTA
tal vez no me explique muy bien que se diga, pero estare actualizando la guía.